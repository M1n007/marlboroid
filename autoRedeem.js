const fetch = require('node-fetch');
const cheerio = require('cheerio');
const fs = require('fs');
const moment = require('moment');
const readlineSync = require('readline-sync');

console.log(`
        Auto Redeem Voucher Marlboro. © Easy To Learn

        - Pastikan Format file akun adalah email|pass
`);

const urlVoucher =  readlineSync.question('Masukan Url Voucher : ');
const fileLocation =  readlineSync.question('Masukan Lokasi File Akun :  ');

function randNumber(length) {
	result = '';
	const characters = '0123456789';
	const charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

const getCookie = () => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/auth/login', {
        method: 'GET',
    }).then(async res => {
        const $ = cheerio.load(await res.text());
        const result = {
            cookie: res.headers.raw()['set-cookie'],
            csrf: $('input[name=decide_csrf]').attr('value')
        }

        resolve(result)
    })
    .catch(err => reject(err))
});

const login = (deviceId, deviceIds, session, csrf, email, pass) => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/auth/login', {
        method: 'POST',
        headers: {
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            cookie: `scs=1; ${deviceId}; _ga=GA1.2.801524997.1569333950; _hjid=410e1028-428b-4119-bd16-4c33345cfce3; accC=true; _gid=GA1.2.956867519.1569892586; kppid_managed=M6zzHlLL; scs=1; ins-gaSSId=19abc7ed-2a04-6af2-78e8-5ac10ba384f1_1569984106; _p1K4r_=true; pikar_redirect=true; token=QzjCgYHq4HaIEetYjL6WlJpYIHutelgx; refresh_token=uhEppIhB0oBVXCmh3NpDEAHtdYZyWYKZ; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216d6397b49c19-0f41926c0d830b-67e1b3f-100200-16d6397b49d35f%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; _gat_UA-102334128-3=1; insdrSV=14; ${session}`,
            host: 'www.marlboro.id',
            origin: 'https://www.marlboro.id',
            referer: 'https://www.marlboro.id/auth/login',
            'sec-fetch-mode': 'cors',
            'x-requested-with': 'XMLHttpRequest',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
        },
        body: `email=${email}&password=${pass}&remember_me=remember_me&ref_uri=/&decide_csrf=${csrf}&param=&exception_redirect=false`

    }).then(async res => {
        const $ = cheerio.load(await res.text());
        const result = {
            cookie: res.headers.raw()['set-cookie'],
        }

        resolve(result)
    })
    .catch(err => reject(err))
});

const checkSession = (deviceId, deviceIds, mm3rm4bre, token, refreshToken, decideSession) => new Promise((resolve, reject) => {
    fetch(`https://www.marlboro.id/aldmic/check-sess/?_=15701826${randNumber(5)}`, {
        method: 'GET',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'Referer': 'https://www.marlboro.id/',
            'Cookie': `${deviceId}; _ga=GA1.2.2144154283.1567004816; _hjid=3b6a983c-3dee-4457-b9a4-02d3ea7a0f26; ins-mig-done=1; accC=true; _hjDonePolls=442481; kppid_managed=M3VnfLAF; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216cd8c3e87a12c-0597b3ea04f5f6-7373e61-1fa400-16cd8c3e87b2ea%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%2C%22%24search_engine%22%3A%20%22google%22%7D; scs=1; ins-gaSSId=01911ce4-313e-26c2-ff1b-2ffc9f33c2e8_1570180807; _gid=GA1.2.33258986.1570180807; ev=1; _p1K4r_=true; pikar_redirect=true; ${mm3rm4bre}; ${token}; ${refreshToken}; insdrSV=75; _gat_UA-102334128-3=1; ${decideSession};`

        }
    })
    .then(res => res.json())
    .then(res => resolve(res))
    .catch(err => reject(err))
});

const getURlAlcdemic = (deviceId, deviceIds, mm3rm4bre, token, refreshToken, decideSession) => new Promise((resolve, reject) => {
    fetch(`https://www.marlboro.id/aldmic/catalog?_=15701826${randNumber(5)}`, {
        method: 'GET',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'Referer': 'https://www.marlboro.id/',
            'Cookie': `${deviceId}; _ga=GA1.2.2144154283.1567004816; _hjid=3b6a983c-3dee-4457-b9a4-02d3ea7a0f26; ins-mig-done=1; accC=true; _hjDonePolls=442481; kppid_managed=M3VnfLAF; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216cd8c3e87a12c-0597b3ea04f5f6-7373e61-1fa400-16cd8c3e87b2ea%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%2C%22%24search_engine%22%3A%20%22google%22%7D; scs=1; ins-gaSSId=01911ce4-313e-26c2-ff1b-2ffc9f33c2e8_1570180807; _gid=GA1.2.33258986.1570180807; ev=1; _p1K4r_=true; pikar_redirect=true; ${mm3rm4bre}; ${token}; ${refreshToken}; insdrSV=75; _gat_UA-102334128-3=1; ${decideSession};`

        }
    })
    .then(async res => {
        const result = {
            cookie: res.headers.raw()['set-cookie'],
            body: await res.json()
        };

        resolve(result)
    })
    .catch(err => reject(err))
});

const getToken = (deviceId, deviceIds, session2, mm3rm4bre) => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/', {
        method: 'GET',
        headers: {
        	Host: 'www.marlboro.id',
        	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
        	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        	'Accept-Language': 'en-US,en;q=0.5',
			'Accept': 'application/json',
			'Accept-Encoding': 'gzip, deflate, br',
			'DNT': 1,    
			'Connection': 'keep-alive',
			'Upgrade-Insecure-Requests': 1,
			cookie: `${deviceId}; _ga=GA1.2.801524997.1569333950; _hjid=410e1028-428b-4119-bd16-4c33345cfce3; accC=true; _gid=GA1.2.956867519.1569892586; kppid_managed=M6zzHlLL; scs=1; ins-gaSSId=19abc7ed-2a04-6af2-78e8-5ac10ba384f1_1569984106; _p1K4r_=true; pikar_redirect=true; ${mm3rm4bre}; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216d6397b49c19-0f41926c0d830b-67e1b3f-100200-16d6397b49d35f%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; insdrSV=16; token=gWaIXv7WEdx3uugu2e7ReJqxDuzptUlX; refresh_token=Ubia8eQnQawAe92hcIAjH4IxFsHUcMh7; ${session2}`
        }
    }).then(async res => {
        const $ = cheerio.load(await res.text());
        const result = {
            cookie: res.headers.raw()['set-cookie'],
        }
        resolve(result)
    })
    .catch(err => reject(err))
});

const getCatalogCookie = (url) => new Promise((resolve, reject) => {
    fetch(url, {
        method: 'GET',
        headers: {
            'Authority': 'loyalty.aldmic.com',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'cross-site',
            'Referer': 'https://www.marlboro.id/profile',
            'Upgrade-Insecure-Requests': 1,
        },
        redirect: 'manual',
        compress: true,
    })
    .then(res => {
        resolve(res.headers.raw()['set-cookie'])
    })
    .catch(err => reject(err))
});

const getTokenVoucher = (cfuid, xsrfToken, aldemicSession, aldmicKey, url) => new Promise((resolve, reject) => {
    fetch(urlVoucher, {
        method: 'GET',
        headers: {
            'host': 'loyalty.aldmic.com',
            'cache-control': 'max-age=0',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-user': '?1',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'sec-fetch-site': 'same-origin',
            'referer': 'https://loyalty.aldmic.com/catalog',
            'cookie': `${cfuid}; ${aldmicKey}; ${xsrfToken}; ${aldemicSession};`,
        },
        redirect: 'manual',
    })
    .then(res => res.text())
    .then(res => {
        const $ = cheerio.load(res);
        const token = $('input[name=_token]').attr('value');
        resolve(token)
    })
    .catch(err => reject(err))
});


const getNewSession = (cfuid, xsrfToken, aldemicSession, aldmicKey, url) => new Promise((resolve, reject) => {
    fetch('https://loyalty.aldmic.com/user', {
        method: 'GET',
        headers: {
            'Host': 'loyalty.aldmic.com',
            'X-Requested-With': 'XMLHttpRequest',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'Referer': urlVoucher,
            'cookie': `${cfuid}; ${aldmicKey}; ${xsrfToken}; ${aldemicSession};`,
        },
        redirect: 'manual',
    })
    .then(async res => {
        const result = {
            body: await res.json(),
            cookie: res.headers.raw()['set-cookie']
        }
        resolve(result)
    })
    .catch(err => reject(err))
});

const getRedirectVoucher = (cfuid, xsrfToken, aldemicSession, aldmicKey, url, tokenVoucher) => new Promise((resolve, reject) => {
    fetch(urlVoucher, {
        method: 'POST',
        headers: {
            'host': 'loyalty.aldmic.com',
            'origin': 'https://loyalty.aldmic.com',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-user': '?1',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'sec-fetch-site': 'same-origin',
            'content-type': 'application/x-www-form-urlencoded',
            'referer': urlVoucher,
            'cookie': `${cfuid}; _ga=GA1.2.409976121.1570185467; _gid=GA1.2.995070374.1570371209; ${aldmicKey}; ${xsrfToken}; ${aldemicSession};`,
        },
        redirect: 'manual',
        compress: true,
        body: `_token=${tokenVoucher}`
    })
    .then(async res => {
        const result = {
            url: res.headers.get('location'),
            cookie: res.headers.raw()['set-cookie'],
            body: await res.text()
        };
        resolve(result)
    })
    .catch(err => reject(err))
});


const getResultVoucher = (cfuid, xsrfToken, aldemicSession, aldmicKey, url) => new Promise((resolve, reject) => {
    fetch(url, {
        method: 'GET',
        headers: {
            'host': 'loyalty.aldmic.com',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-user': '?1',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
            'sec-fetch-site': 'same-origin',
            'referer': urlVoucher,
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
            'cookie': `${cfuid}; ${xsrfToken}; ${aldemicSession}; ${aldmicKey};`,
        }
    })
    .then(res => res.text())
    .then(res => {
        const $ = cheerio.load(res);
        const result = $('div span div.popup-body p').text()
        resolve(result)
    })
    .catch(err => reject(err))
});

(async () => {
    // await delay(50000); //delay satuan milisecond 1000ms = 1 second, 10000ms = 10 second
    fs.readFile(fileLocation, async function (err, data) {
        if (err) throw err;
        const array = data
        .toString()
        .replace(/\r\n|\r|\n/g, " ")
        .split(" ");
        
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            if (element) {
                const email = element.split('|')[0];
                const pass = element.split('|')[1];
                console.log('');
                console.log('');
                console.log(`[${moment().format("HH:mm:ss")}] Mengambil Cookie...`);
                const cookie = await getCookie();
                const cok = cookie.cookie.join().split(',');
                const deviceId = cok[0].split(';')[0];
                const deviceIds = cok[2].split(';')[0];
                const session = cok[4].split(';')[0];
                const csrf = cookie.csrf;
                console.log(`[${moment().format("HH:mm:ss")}] Mencoba Login...`);
                const log = await login(deviceId, deviceIds, session, csrf, email, pass);
                const cokk = log.cookie.join().split(',');
                const mm3rm4bre = cokk[4].split(';')[0];
                const session2 = cokk[6].split(';')[0];
                console.log(`[${moment().format("HH:mm:ss")}] Mengambil Token...`);
                const token = await getToken(deviceId, deviceIds, session2, mm3rm4bre);
                const tokenn = token.cookie.join().split(',');
                const tokennn = tokenn[2].split(';')[0];
                const refresh_token = tokenn[4].split(';')[0];
                const session3 = tokenn[6].split(';')[0];
                console.log(`[${moment().format("HH:mm:ss")}] Mengecek Session...`);
                const checkSessions = await checkSession(deviceId, deviceIds, mm3rm4bre, tokennn, refresh_token, session3);
                if (checkSessions.data.code === 1) {
                    console.log(`[${moment().format("HH:mm:ss")}] Session dapat Diakses...`);
                    console.log(`[${moment().format("HH:mm:ss")}] Mengambil Url Aldmic...`);
                    const getUrlAlcdemics = await getURlAlcdemic(deviceId, deviceIds, mm3rm4bre, tokennn, refresh_token, session3);
                    const url = getUrlAlcdemics.body.data.url;
                    if (url) {
                        console.log(`[${moment().format("HH:mm:ss")}] Sukses Mengambil Url : ${url}`);
                        const catalogCookie = await getCatalogCookie(url);
                        const newCookie = catalogCookie.join().split(',');
                        const cfuid = newCookie[0].split(';')[0];
                        const xsrf_token = newCookie[2].split(';')[0];
                        const aldmic_session = newCookie[4].split(';')[0];
                        const aldmic_key = newCookie[6].split(';')[0];
                        console.log(`[${moment().format("HH:mm:ss")}] Get Token Voucher...`);
                        const tokenVoucher = await getTokenVoucher(cfuid, xsrf_token, aldmic_session, aldmic_key, '');
                        if (tokenVoucher) {
                            console.log(`[${moment().format("HH:mm:ss")}] Berhasil mengambil token voucher : ${tokenVoucher}`);
                            const generateNewSession = await getNewSession(cfuid, xsrf_token, aldmic_session, aldmic_key, '');
                            console.log(`[${moment().format("HH:mm:ss")}] Profile Name : ${generateNewSession.body.data.name}`);
                            console.log(`[${moment().format("HH:mm:ss")}] Point Tersisa : ${generateNewSession.body.data.point}`);
                            const genNewSessionCookie = generateNewSession.cookie.join().split(',');
                            const newSessionXsrfToken = genNewSessionCookie[0].split(';')[0];
                            const newSessionVoucher = genNewSessionCookie[2].split(';')[0];
                            console.log(`[${moment().format("HH:mm:ss")}] Mencoba mengambil url redirect..`);
                            const getUrlVoucher = await getRedirectVoucher(cfuid, newSessionXsrfToken, newSessionVoucher, aldmic_key, '', tokenVoucher);
                            if (getUrlVoucher.url) {
                                console.log(`[${moment().format("HH:mm:ss")}] Sukses mengambil url : ${getUrlVoucher.url}`);
                                console.log(`[${moment().format("HH:mm:ss")}] Mencoba Redeem...`);
                                const getRedeemMessage = await getResultVoucher(cfuid, newSessionXsrfToken, newSessionVoucher, aldmic_key, getUrlVoucher.url);
                                console.log(`[${moment().format("HH:mm:ss")}] ${getRedeemMessage}`);
                            }else{
                                const $ = cheerio.load(getUrlVoucher.body);
                                const text = $('div.popup-body p').text();
                                console.log(`[${moment().format("HH:mm:ss")}] Gagal Mengambil Url, Message :  ${text}`);
                                console.log('');
                                console.log('');
                            }                
                        }else{
                            console.log(`[${moment().format("HH:mm:ss")}] Gagal Mendapatkan Voucher Token`);
                            console.log('');
                            console.log('');
                        }
                    }else{
                        console.log(`[${moment().format("HH:mm:ss")}] Gagal Mengambil Url Aldmic`);
                        console.log('');
                        console.log('');
                    }
                }

            }
        }
    })
    
})();