const {AntiCaptcha} = require('anticaptcha');
const dbc = require('./deadbycaptcha');
 
const mainProcess = async (client_id) => {
    const AntiCaptchaAPI = new AntiCaptcha(client_id); // You can pass true as second argument to enable debug logs.
    // Checking the account balance before creating a task. This is a conveniance method.
    if (await !AntiCaptchaAPI.isBalanceGreaterThan(10)) {
        // You can dispatch a warning using mailer or do whatever.
        console.warn("Take care, you're running low on money !")
    }
 
    // Creating a task to resolve.
    const taskId = await AntiCaptchaAPI.createTask(
        "https://www.marlboro.id/auth/register", // The page where the captcha is
        "6LfFZpEUAAAAAAOeeFUdj-v_pUMb28yoq6SyjBta", // The data-site-key value
    )
 
    // Waiting for resolution and do something
    const response = await AntiCaptchaAPI.getTaskResult(taskId);
    return response;
}

const dbcProccess = async (usernames, passwords) => new Promise((resolve, reject) =>  {

    const username = usernames;     // DBC account username
    const password = passwords;     // DBC account password

    // Proxy and Recaptcha token data
    const token_params = JSON.stringify({
        'proxy': 'http://username:password@proxy.example:3128',
        'proxytype': 'HTTP',
        'googlekey': '6LfFZpEUAAAAAAOeeFUdj-v_pUMb28yoq6SyjBta',
        'pageurl': 'https://www.marlboro.id/auth/register'
    });

    // Death By Captcha Socket Client
    const client = new dbc.SocketClient(username, password);


    // Solve captcha with type 4 & token_params extra arguments
    client.decode({extra: {type: 4, token_params: token_params}}, (captcha) => {
        if (captcha) {
            resolve(captcha['text'])
        }
    });

})

module.exports = {
    GetGCaptcha: mainProcess,
    dbcProccess
}