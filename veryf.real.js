const fetch = require('node-fetch');
const cheerio = require('cheerio');
const fs = require('fs');
const moment = require('moment');
const delay = require('delay');

function randstr(length) {
	result = '';
	const characters = '012345678910abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	const charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}


const functionGetLink = (email, domain) =>new Promise((resolve, reject) => {
    fetch(`https://generator.email/${domain}/${email}`, {
        method: "get",
        headers: {
            accept:
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
            "accept-encoding": "gzip, deflate, br",
            cookie: `_ga=GA1.2.659238676.1567004853; _gid=GA1.2.273162863.1569757277; embx=%5B%22${email}%40${domain}%22%2C%22hcycl%40nongzaa.tk%22%5D; _gat=1; io=io=tIcarRGNgwqgtn40O${randstr(3)}; surl=${domain}%2F${email}`,
            "upgrade-insecure-requests": 1,
            "user-agent":
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36"
        }
    })
        .then(res => res.text())
        .then(text => {
            const $ = cheerio.load(text);
            const src = $("a[name=staging_marlboro_id_auth_verify_ema]").attr('href');
            resolve(src);
        })
        .catch(err => reject(err));
});

const functionVeryf = (url) => new Promise((resolve, reject) => {
    fetch(url, {
        method: "get"
    })
        .then(res => res.text())
        .then(text => {
            const $ = cheerio.load(text);
            const src = $("p.staticp__text.mb50").text();
            resolve(src);
        })
        .catch(err => reject(err));
});




(async () => {
    fs.readFile('link.txt', async function (err, data) {
        if (err) throw err;
        const array = data
        .toString()
        .replace(/\r\n|\r|\n/g, " ")
        .split(" ");

        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            if (element) {
                const uname = element.split('|')[0];
                const domain = element.split('|')[1];
                console.log(`[${moment().format("HH:mm:ss")}] Email : ${uname}@${domain}`);
                await delay(30000);
                const getLink = await functionGetLink(uname, domain);
                if (getLink) {
                    console.log(`[${moment().format("HH:mm:ss")}] Berhasil mendapatkan link : ${getLink}`);
                    console.log(`[${moment().format("HH:mm:ss")}] Mencoba Mem-Verifikasikan Email.`);
                    const veryf = await functionVeryf(getLink)
                    console.log(`[${moment().format("HH:mm:ss")}] Sukses : ${veryf} `);
                    console.log('');
                    console.log('');
                }else{
                    console.log(`[${moment().format("HH:mm:ss")}] Email sudah terverifikasi / tidak menemukan link.`);
                    console.log('');
                    console.log('');
                }
            }
            
        }
    })

})();